    <header class="warna_base">
      <div class="container-fluid" >
        <div class="row h-first-row " style="background:#444;color:#fff">
          <div class="m1170 ">
            <div class="col-xs-5 text-uppercase">
              {{ get_option('site_title') }} | @if(get_option('site_phone_number'))
              <i class="fa fa-phone"></i>
              +{{ get_option('site_phone_number') }} 
              @endif </div>
              <div class="col-xs-7 text-right">
                <div class="row">
                  <div class="h-curr col-sm-5">
                    @if(get_option('site_email_address'))
                    <i class="fa fa-envelope"></i>
                    {{ get_option('site_email_address') }}
                    @endif
                  </div>
                  <div class="h-login col-sm-7" >

                    <div class="topContactInfo"> @if(Auth::check())
                      <ul class="nav nav-pills navbar-right" >
                       <a href="{{ route('profile') }}" style="color:#fff">
                        <i class="fa fa-user"></i>
                        @lang('app.hi'), {{ $logged_user->name }} </a>&nbsp;&nbsp;
                        <a href="{{ route('dashboard') }}" style="color:#fff">
                          <i class="fa fa-dashboard"></i>
                          Dashboard </a>&nbsp;&nbsp;
                          <a href="{{ route('logout') }}" style="color:#fff">
                            <i class="fa fa-sign-out"></i>
                            @lang('app.logout')</a>
                          </ul>
                          @else
                          <ul class="nav nav-pills navbar-right" >
                           <a href="{{ route('login') }}" style="color:#fff">
                            <i class="fa fa-user"></i>
                            Login </a>&nbsp;&nbsp;
                            <a href="{{ route('register') }}" style="color:#fff">
                              <i class="fa fa-plus-square"></i>
                              Daftar </a>&nbsp;&nbsp;
                            </ul>@endif
                          </div>
                        </div>          
                      </div>          
                    </div>
                  </div>
                </div>
                <div class="row bg-search">
                  <div id="header" class=" h-second-row">
                    <div class="row row1">
                     <ul class="largenav pull-right">
                      @if($header_menu_pages->count() > 0)
                      @foreach($header_menu_pages as $page)
                      <li class="upper-links">

                       <a class="links" href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a>

                     </li>@endforeach
                     @endif
                     <li class="upper-links"><a class="links" href="{{ route('register') }}">Pendaftaran </a>
                     </li>
                     <li class="upper-links"><a class="links" href="{{ route('blog') }}">Blog</a></li>
                     <li class="upper-links"><a class="links" href="{{ route('contact_us_page') }}">Hubungi Kami</a></li>

                     <li class="upper-links dropdown"><a class="links" href="http://clashhacks.in/">Panduan</a>
                      <ul class="dropdown-menu">

                       <li class="profile-li"><a class="profile-links" href="{{ route('contact_us_page') }}">Tentang Kami</a></li>
                       <li class="profile-li"><a class="profile-links" href="{{ route('register') }}">Pendaftaran </a>
                       </li>
                       <li class="profile-li"><a class="profile-links" href="{{ route('register') }}">Panduan </a>
                       </li>
                       <li class="profile-li"><a class="profile-links" href="{{ route('blog') }}">Blog</a></li>
                       <li class="profile-li"><a class="profile-links" href="{{ route('contact_us_page') }}">Hubungi Kami</a></li>
                     </ul>
                   </li>
                 </ul>

               </div>

               <div id="flipkart-navbar">
                <div class="#">

                 <div class="col-sm-12 row2">
                  <div class="col-sm-3" style="padding-right:14px">
                   <a href="{{ route('home') }}"> <img src="{{ asset('assets/tema/bazzarukm/img/bazzarukm_white-02.png') }}" style="max-height:40px;"></a>

                 </div>
                 <div class="flipkart-navbar-search smallsearch col-sm-7 col-xs-11" >
                  <form class="form-inline" action="{{ route('listing') }}" method="get">
                    <div class="clear" >
                      <input class="flipkart-navbar-input col-xs-11" id="searchTerms" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')"   name="">
                      <button class="flipkart-navbar-button col-xs-1">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
                <div class="cart largenav col-sm-2">
                  <a class="cart-button" style="color:#fff">
                   <i class="fa fa-plus"></i> Upload Produk
                 </a>
               </div>
             </div>
           </div>
         </div>


       </div>
     </div></div>  
    </header>
    <center><strong><h3>#bazzarukm #bazzar #produkukm #bazzarmurah #bazzarproduk</h3></strong> </center>
<div class="row"><div class="col-sm-8 col-sm-offset-2 top10 text-center"><p>
      Sebuah komunitas ekonomi usaha kecil menengah dengan mengedepankan produk produk umkm 
      dan ukm dari seluruh pelosok nusantara dengan harapan tumbuh setiap kecamatan mempunyai produk produk unggulan yang bisa bersaing 
      dengan usaha usaha untuk kebutuhan sehari hari maupun untuk kebuhuhan sekunder. Demi membangkitkan ekonomi yang berkemajuan untuk 
      membangun Negara Kesatuan Republik Indonesia  </p></div></div>
  