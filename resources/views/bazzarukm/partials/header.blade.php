    <!DOCTYPE html>
    <html class="no-js" lang="ID">  
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@section('title') {{ get_option('site_title') }} @show</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @section('social-meta')
        <meta property="og:title" content="{{ get_option('site_title') }}">
        <meta property="og:description" content="{{ get_option('meta_description') }}">
        <meta property="og:url" content="{{ route('home') }}">
        <meta name="twitter:card" content="summary_large_image">
        <!--  Non-Essential, But Recommended -->
        <meta name="og:site_name" content="{{ get_option('site_name') }}">
        @show 
        <link href="{{ asset('assets/tema/bazzarukm/css/master.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/tema/bazzarukm/assets/switcher/css/color1.css') }}" rel="stylesheet" type="text/css">
 <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">         <!-- slick carousel -->
        <link href="{{ asset('assets/tema/bazzarukm/js/slick-theme.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/tema/bazzarukm/js/slick.css') }}" rel="stylesheet" type="text/css">
                @yield('page-css')

    </head>
    <body>
    <section class="shadow_boxed" style="margin:3% 10% 3% 10%">
        @include('bazzarukm.partials.navigasi') 

      