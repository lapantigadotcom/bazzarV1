    <footer>
        <div class="container-fluid">
            <div class="row f-second-row footer">
                <div class="m1170">
                    <div class="col-sm-4 f-1td top10" data-wow-delay="0.7s" data-wow-duration="1.5s">
                        <h5 class="uppercase colorffd4 footer-title"><b class="footer-title">Tentang Kami</b></h5>
                        <div class="footer-logo top30">
                          <img src="{{ asset('assets/tema/bazzarukm/img/bazzarukm_white-02.png') }}" style="max-height:50px"></div>
                          <div class="footer-td1-txt coloraaa top30 p-right20">
                            <p>{{ get_option('footer_about_us') }}</p>
                         </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-4 f-2td top10" >
                        <h5 class="uppercase colorffd4"><b class="footer-title">Tautan Kami</b></h5>
                        <ul class="list-styles left-40 top30 f-left coloraaa footer-list p-right20">
                             
                            @if($show_in_footer_menu->count() > 0)
                                @foreach($show_in_footer_menu as $page)
                                    <li><a href="{{ route('single_page', $page->slug) }}"><i class="fa icon-chevron-right">&nbsp;</i> {{ $page->title }} </a></li>
                                @endforeach
                            @endif

                            @if(get_option('show_blog_in_footer'))
                                <li><a href="{{ route('blog') }}"><i class="fa icon-chevron-right">&nbsp;</i>@lang('app.blog')</a></li>
                            @endif
                            <li><a href="{{ route('contact_us_page') }}"><i class="fa icon-chevron-right">&nbsp;</i>@lang('app.contact_us')</a></li>
                        </ul>
                        <ul class="list-styles left-40 top30 f-left">
                            <li><a href="#"><i class="fa icon-chevron-right">&nbsp;</i>Pendaftaran</a></li>
                            <li><a href="#"><i class="fa icon-chevron-right">&nbsp;</i>Panduan</a></li>
                            <li><a href="#"><i class="fa icon-chevron-right">&nbsp;</i>FAQ</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-4 f-3td top10" >
                        <h5 class="uppercase colorffd4"><b class="footer-title">Kontak Kami</b></h5>
                        <div class="top30 p-right20 coloraaa">
                            <b class="uppercase colorfff">{{ get_text_tpl(get_option('footer_company_name')) }}</b>
                            @if(get_option('footer_address'))
                            <br />
                            <i class="fa fa-map-marker"></i>
                            {!! get_option('footer_address') !!}
                            @endif
                            <table>
                                <tr><td> <i class="fa fa-phone"></i> Phone (Hunting)
                                    @if(get_option('site_phone_number'))     {!! get_option('site_phone_number') !!}  </td>    
                                         
                                        @endif</td></tr>
                                        @if(get_option('site_email_address')) <tr><td><i class="fa fa-envelope-o"></i> E-mail:  {{ get_option('site_email_address') }} </td></tr>
                                        @endif              
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row f-third-row footer2">
                        <div class="ft10">
                            <div class="footer-relative">
                                <div class="col-sm-6 left-footer2" style="padding-top:5px">
                                    &copy; 2017 {{ get_text_tpl(get_option('footer_company_name')) }} - All Rights Reserved. <a href="#">Privacy Policy </a>
                                </div>
                                <div class="col-sm-6 right-footer2" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                      Best view on 1024x800 
                                     
                                    <a href="#">
                                        <div class="img-icons-2"><i class="fa fa-facebook">&nbsp;</i></div>
                                    </a>
                                    <a href="#">
                                        <div class="img-icons-2"><i class="fa fa-google-plus">&nbsp;</i></div>
                                    </a>
                                    <a href="#">
                                        <div class="img-icons-2"><i class="fa fa-twitter">&nbsp;</i></div>
                                    </a>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
                <a id="to-top" href="#header-top"><i class="fa fa-chevron-up"></i></a>
    </footer> 

        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!-- slick js -->
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/slick.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/modernizr.custom.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/classie.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/bootstrap-slider.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/bootstrap-datepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/assets/slider-pro/js/jquery.sliderPro.min.js') }}"></script>
        <!--Theme-->
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/jquery.smooth-scroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/wow.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/jquery.placeholder.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/tema/bazzarukm/js/theme.js') }}"></script> 
        @yield('page-js')

           <script type="text/javascript">
        $('.slider-pro').slick({
  lazyLoad: 'ondemand',
          arrows: false,
  slidesToShow: 5,
  dots:true,
   focusOnSelect: false,
  slidesToScroll: 1
});
</script></section> 
    </body>
    </html>