 <div class="row header-slider">     
                    <div class="main-slider">
                        <div class="slider-pro full-width-slider" id="main-slider" data-width="100%" data-height="650" data-fade="true" data-buttons="false" data-wait-for-layers="true" data-thumbnail-pointer="false" data-touch-swipe="false" data-autoplay="true" data-auto-scale-layers="true" data-visible-size="100%" data-force-size="fullWidth" data-autoplay-delay="5000" >
                            <div class="sp-slides">
                                <!-- Slide 1 -->
                                <div class="sp-slide">
                                    <img class="sp-image" src="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}"  data-src="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}" data-retina="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}" alt="Image"/>
                                    <div class="sp-desc-container">
                                        <div class="sp-layer font42 semibold uppercase text-center" data-vertical="30%" data-show-transition="up" data-hide-transition="up" data-show-delay="600" data-hide-delay="400">trusted by millions</div>
                                        <div class="sp-layer font28 light text-center" data-vertical="45%" data-show-transition="up" data-hide-transition="up" data-show-delay="800" data-hide-delay="600">We help new customers & search engines to find your business online</div>
                                    </div>
                                </div>

                                <!-- Slide 2 -->
                                <div class="sp-slide">
                                    <img class="sp-image" src="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}"  data-src="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}" data-retina="{{ asset('assets/tema/bazzarukm/media/backgrounds/home2-bg.jpg') }}" alt="Image"/>
                                    <div class="sp-desc-container">
                                        <div class="sp-layer font42 semibold uppercase text-center" data-vertical="30%" data-show-transition="left" data-hide-transition="left" data-show-delay="600" data-hide-delay="400">trusted by millions</div>
                                        <div class="sp-layer font28 light text-center" data-vertical="45%" data-show-transition="left" data-hide-transition="left" data-show-delay="800" data-hide-delay="600">We help new customers & search engines to find your business online</div>
                                    </div>
                                </div>

                                 
                            </div>
                        </div>
                        <div class="home-2-filter m1170 pl-15 pr-15" data-wow-delay="0.7s" data-wow-duration="1.5s">                
                            <div class="make-reservation home-reservation">
                                <div class="reservation-dropdowns top40">
                                    <div class="col-sm-3 p-left0 p-right0">
                                        <div class="btn-group width100">
                                            <input class="dropdown-btn-list dropdown-btn1 dropdown-btn1-1 btn btn-lg dropdown-toggle" type="text" placeholder="keywords" onclick="this.focus()">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 p-left0 p-right0">
                                        <div class="btn-group width100">
                                            <div class="custom-select select-type4 high location">
                                                <select class="chosen-select">
                                                    <option value="1">- Choose Location -</option>
                                                    <option value="2">USA</option>
                                                    <option value="3">Ukraine</option>
                                                    <option value="4">France</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 p-left0 p-right0">
                                        <div class="btn-group width100">
                                            <div class="custom-select select-type4 high category">
                                                <select class="chosen-select">
                                                    <option value="1">- Choose Category -</option>
                                                    <option value="2">Some Category</option>
                                                    <option value="3">Category Listings</option>
                                                    <option value="4">Other Category</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 p-left0 p-right0">
                                        <div class="btn-group width100">
                                            <a href="#"><div class="find-btn">Search</div></a>
                                        </div>
                                    </div><div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>