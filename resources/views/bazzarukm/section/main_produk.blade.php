                <div class="wrapper row">
                    <div class="preview col-md-6">


                        @if( ! empty($ad->video_url))
                        <?php
                        $video_url = $ad->video_url;
                        if (strpos($video_url, 'youtube') > 0) {
                            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_url, $matches);
                            if ( ! empty($matches[1])){
                                echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$matches[1].'" frameborder="0" allowfullscreen></iframe></div>';
                            }

                        } elseif (strpos($video_url, 'vimeo') > 0) {
                            if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $video_url, $regs)) {
                             if (!empty($regs[3])){
                                 echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$regs[3].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
                             }
                         }
                     }
                     ?>

                     @else

                     <div class="ads-gallery">
                        <div class="fotorama"  data-nav="thumbs" data-allowfullscreen="true" data-width="100%">
                            @foreach($ad->media_img as $img)
                            <img src="{{ media_url($img, true) }}" alt="{{ $ad->title }}">
                            @endforeach
                        </div>
                    </div>

                    @endif

                    <h4 class="price">Harga<span> {{ themeqx_price_ng($ad->price) }}</span></h4>
 
                    <div class="action">
                        @if($ad->user->email)
                            <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#replyByEmail">
                                <i class="fa fa-envelope-o"> </i> @lang('app.reply_by_email')
                            </button>
                        @endif
                         <button class="btn btn-success btn-sm" type="button" href="javascript:;" id="save_as_favorite" data-slug="{{ $ad->slug }}"> 
                                    @if( ! $ad->is_my_favorite())
                                        <i class="fa fa-bookmark"></i> @lang('app.save_ad_as_favorite')
                                    @else
                                        <i class="fa fa-bookmark-o"></i> @lang('app.remove_from_favorite')
                                    @endif
                                 </button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="details col-md-6">
                    <h3 class="product-title">{{ $ad->title }}</h3>
                    <p class="vote text-muted">
                       @if($ad->category)
                       <i class="fa fa-folder-o"></i><a href="{{ route('listing', ['category' => $ad->category->id]) }}">  {{ $ad->category->category_name }} </a> |
                       @endif
                       @if($ad->brand)
                       <i class="fa fa-industry"></i><a href="{{ route('listing', ['brand' => $ad->brand->id]) }}">  {{ $ad->brand->brand_name }} </a> |
                       @endif
                       <i class="fa fa-map-marker"></i> {!! $ad->full_address() !!} |  <i class="fa fa-clock-o"></i> {{ $ad->posting_datetime()  }}
                   </p>
                   <div class="rating">

                    <span class="review-no">
                    <i class="fa fa-check-circle-o"></i> @lang('app.condition')</strong> {{ $ad->ad_condition }} 
                    </span>
                </div>
                <p class="product-description">{!! nl2br($ad->description) !!} </p>

            </div>
        </div>

