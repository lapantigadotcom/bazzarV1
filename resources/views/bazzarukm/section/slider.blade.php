
<div class="row  home-1-reserv-row2" data-wow-delay="0.7s" data-wow-duration="1.5s">
    <div class="m1170 home-1-reserv">    
        <div class="reserv-header font25 uppercase text-center colorfff"><i class="font22 fa fa-search p-right20"></i> search any business</div>    
        <div class="make-reservation b-radius3 home-reservation2">
            <div class="reservation-dropdowns top60 p-top30">
                <div class="col-sm-3 p-left0 p-right0" style="height: 48px;">
                    <div class="open-close-filter f-left"><i class="fa fa-navicon font20 colorfff">&nbsp;</i></div>
                    <div class="btn-group f-right" style="width: calc(100% - 30px);">
                        <input class="dropdown-btn-list dropdown-btn1 dropdown-btn1-1 btn btn-lg dropdown-toggle" placeholder="keywords" type="text">
                    </div>
                </div>
                <div class="col-sm-3 p-left0 p-right0">
                    <div class="btn-group width100">
                        <div class="custom-select select-type4 location">
                            <select class="chosen-select" style="display: none;">
                                <option value="1">- Choose Location -</option>
                                <option value="2">USA</option>
                                <option value="3">Ukraine</option>
                                <option value="4">France</option>
                            </select><div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 100%;" title=""><a class="chosen-single" tabindex="-1"><span>- Choose Location -</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" readonly="" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 p-left0 p-right0">
                    <div class="btn-group width100">
                        <div class="custom-select select-type4 category">
                            <select class="chosen-select" style="display: none;">
                                <option value="1">- Choose Category -</option>
                                <option value="2">Some Category</option>
                                <option value="3">Category Listings</option>
                                <option value="4">Other Category</option>
                            </select><div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 100%;" title=""><a class="chosen-single" tabindex="-1"><span>- Choose Category -</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input autocomplete="off" readonly="" type="text"></div><ul class="chosen-results"></ul></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 p-left0 p-right0">
                    <div class="btn-group width100">
                        <a href="#"><div class="find-btn">Search</div></a>
                    </div>
                </div> 
            </div>
        </div>



    </div>

</div>  

