<div class="col-sm-12  top40">
<div class="row row-browse-categories p-bottom40">
    <div class="m1170">
         
        @foreach($top_categories as $category)
        <div class="col-sm-4">
            <div class="table-categories table-cat-bgf wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.7s; animation-name: fadeInUp;">
                <div class="cat-header"><a href="{{ route('listing') }}?category={{$category->id}}"><i class="fa {{ $category->fa_icon }}"></i> {{ $category->category_name }}</a></div>
                <table class="table-cat-inf">@if($category->sub_categories->count())
                    <tbody>
                       @foreach($category->sub_categories as $s_cat)
                       <tr><td class="cat-td1">
                        <a href="{{ route('listing') }}?category={{$category->id}}&sub_category={{$s_cat->id}}"><i class="fa fa-angle-right">&nbsp;</i>{{ $s_cat->category_name }}</a>
                    </td><td class="cat-td2 cat-header">[{{ number_format($category->product_count) }}]</td>
                </tr> @endforeach

            </tbody>@endif</table>
        </div>

    </div>@endforeach


</div>
</div>
<center>
@if($enable_monetize)
{!! get_option('monetize_code_below_search_bar') !!}
@endif</center>
</div> 