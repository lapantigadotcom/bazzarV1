 
 @if($premium_ads->count() > 0)
 

           <div class="col-md-12">
            <div class="owl-carousel-3col" data-dots="true">
              @foreach($premium_ads as $ad)
              <div class="item">
                <article class="post clearfix maxwidth600 mb-sm-30">
                  <div class="entry-header">
                    <div class="post-thumb thumb"> <img src="{{ media_url($ad->feature_img) }}" alt="{{ $ad->title }}" class="img-responsive img-fullwidth"> </div>
                    <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                    <div class="display-table">
                      <div class="display-table-cell">
                        <ul>
                          <li><a class="text-white" href="#"><i class="fa fa-thumbs-o-up"></i> 265 <br> Likes</a></li>
                          <li class="mt-20"> @if($ad->city)
                                            <a class="text-white" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-location-arrow"></i> {{ $ad->city->city_name }} </a>
                                        @endif</a></li>
                        </ul>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="entry-content border-1px p-20">
                    <h5 class="entry-title mt-0 pt-0"><a href="{{ route('single_ad', [$ad->id, $ad->slug]) }}">{{ str_limit($ad->title, 40) }} </a></h5>
                    <p class="text-left mb-20 mt-15 font-13"> @if($ad->price_plan == 'premium')<a class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left" href="{{ route('single_ad', [$ad->id, $ad->slug]) }}">
                                      {{ ucfirst($ad->price_plan) }} - {{ themeqx_price_ng($ad->price, $ad->is_negotiable) }} 
                                    </a>@endif</p>
                    
                    <ul class="list-inline entry-date pull-right font-12 mt-5">
                      <li>@if($ad->category)<a class="text-theme-colored" href="#">{{ $ad->category->category_name }}</a>@endif</li>
                      <li><span class="text-theme-colored"><i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</span></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
              @endforeach
            </div>
          </div>
        @if($enable_monetize)
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        {!! get_option('monetize_code_below_premium_ads') !!}
                    </div>
                </div>
            </div>
        @endif
 
 @endif
   