<hr>@if($regular_ads->count() > 0)
<div class="row row-explore-nerby blue-title bgbl p-bottom40" style="margin:0">
 		<div class="col-sm-12 ">
			<div class="about-title extrabold uppercase colorfff top10 btm5">
				<span class="bgbl wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Produk UKM Terbaru</span>
				<center><hr style="max-width:300px;"></center>
			</div>
			<div class="font14 bgbl text-center wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Temukan produk UKM terbaru disekitar Anda.</div>
			<div class="slider-pro">	
				@foreach($regular_ads as $ad)
				<div class="col-sm-3 top20 f-left" data-wow-delay="0.7s" data-wow-duration="1.5s">
					<div class="member-prof">
						<div class="memb-photo memb-photo2">
							<a href="{{ route('single_ad', [$ad->id, $ad->slug]) }}">
								<img  data-lazy="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}"></a>		
													<div class="memb-photo-hover memb-photo-hover2">
								<div class="memb-photo-links">
									<br />
									<ul class="list-styles raitings-stars p-left10 inl-flex">
										 
									</ul>
								</div>
							</div>
						</div>
						<div class="memb-txt"> @if($ad->category)
							<span class="font11 semibold uppercase blue">{{ $ad->category->category_name }}</span><br />@endif
							<b class="font14 color333 uppercase"><a title="{{$ad->title}}" href="{{ route('single_ad', [$ad->id, $ad->slug]) }}">{{ str_limit($ad->title, 20) }}</a></b><br />
							<span class="font12 color777">{{ $ad->created_at->diffForHumans() }}</span>
							<div class="font12 color777 top10"><i class="fa fa-phone"></i> <b>{{ themeqx_price_ng($ad->price, $ad->is_negotiable) }}</b></div>
						</div>
					</div>
				</div>@endforeach</div>	
			</div>
		</div>
	</div>
 </div>	
 @endif
       <center>
       	<div class=" wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s" style="background:#fff;visibility: visible; animation-duration: 1.5s; 
       	animation-delay: 0.7s; animation-name: fadeInUp;">
       	<img src="{{ asset('assets/tema/bazzarukm/img/iklan/11373304612482844138.gif') }}" class="img-responsive">
      </div></center>