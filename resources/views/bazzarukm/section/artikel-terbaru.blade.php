 @if(get_option('show_latest_blog_in_homepage') ==1)
<!-- Section: artikel baru -->
    <section>
      <div class="container pt-60">
        <div class="section-title mb-0">
          <div class="row">
            <div class="col-md-8">
              <h2 class="mt-0 text-uppercase font-28 line-bottom">Blog <span class="text-theme-color-2 font-weight-400">Terbaru</span></h2>
              <h4 class="pb-20">artikel, informasi & berita Bazzar UKM terbaru</h4>
           </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row multi-row-clearfix">
            @foreach($posts as $post)
            <div class="col-sm-6 col-md-3 mb-sm-30 sm-text-center">
              <div class="team maxwidth400">
                <div class="thumb">
                    @if($post->feature_img)
                    <img class="img-fullwidth" alt="{{ $post->title }}" src="{{ media_url($post->feature_img) }}">
                @else
                    <img class="img-fullwidth" alt="{{ $post->title }}" src="{{ asset('uploads/placeholder.png') }}">
                @endif 
                </div>
                <div class="content border-1px p-15 bg-light clearfix">
                  <h4 class="name text-theme-color-2 mt-0">{{ str_limit(strip_tags($post->title), 20) }} <small><br><i class="fa fa-calendar"></i> {{ $post->created_at_datetime() }}
                    <br>@if($post->author) <a href="{{ route('author_blog_posts', $post->author->id) }}"><i class="fa fa-user"></i> {{ $post->author->name }}</a> 
                        @endif
                  </small></h4>
                  <p class="mb-20"> {{ str_limit(strip_tags($post->post_content), 50) }}</p>
                     
                   <a class="btn btn-theme-colored btn-sm pull-right flip" href="{{ route('blog_single', $post->slug) }}">selengkapnya</a>
                </div>
              </div>
            </div>
             @endforeach
          </div>
        </div>
      </div>
    </section> 
    @endif   
                        
