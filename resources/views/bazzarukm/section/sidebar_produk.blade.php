
<div class="profile-sidebar">
    <!-- SIDEBAR USERPIC -->
    <div class="profile-userpic">
        <img src="{{ $ad->user->get_gravatar() }}" class="img-circle img-responsive" />
    </div>
    <div class="profile-usertitle">
        <div class="profile-usertitle-name">
           {{ $ad->user->name }}
        </div>
        <div class="profile-usertitle-job">
        	 @if($ad->user->email)
            {{ $ad->user->get_address()}}
            @endif
        </div>
    </div>
    
    <div class="profile-userbuttons">
         
          <button type="button" class="btn btn-success btn-sm" id="onClickShowPhone">
                             No. Tlp&nbsp;<span id="ShowPhoneWrap">&nbsp; (click to view)</span>  
                            
                        </button>
     </div>
    
    <div class="profile-usermenu">
        <ul class="nav">
            <li class="active">
                <a href="#">
                   <i class="fa fa-money"></i>  </strong> {{ themeqx_price_ng($ad->price) }} </a>
                </li>
                 
                   
                        <li>
                            <a href="#">
                                <i class="fa fa-question-circle"></i>
                                Help </a>
                            </li>
                            <li><a href="#">
                                 <i class="fa fa-exclamation-circle"></i>
                                Tips BazzarUKM   
                                <p style="padding-left:25px">Disarankan untuk COD
                                    <br>Bayar setelah produk / item diterima</p></a>
                            </li>

                        </ul>
                    </div>
                 </div> 