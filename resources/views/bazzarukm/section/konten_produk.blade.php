 <div class="container">
            <div class="row">
                <div class="modern-single-ad-top-description">

                    <div class="col-sm-7 col-xs-12">
                        @if ( ! $ad->is_published())
                            <div class="alert alert-warning"> <i class="fa fa-warning"></i> @lang('app.ad_not_published_warning')</div>
                        @endif


                            @if( ! empty($ad->video_url))
                                <?php
                                $video_url = $ad->video_url;
                                if (strpos($video_url, 'youtube') > 0) {
                                    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_url, $matches);
                                    if ( ! empty($matches[1])){
                                        echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$matches[1].'" frameborder="0" allowfullscreen></iframe></div>';
                                    }

                                } elseif (strpos($video_url, 'vimeo') > 0) {
                                    if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $video_url, $regs)) {
                                       if (!empty($regs[3])){
                                           echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$regs[3].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
                                       }
                                    }
                                }
                                ?>

                            @else

                                <div class="ads-gallery">
                                    <div class="fotorama"  data-nav="thumbs" data-allowfullscreen="true" data-width="100%">
                                        @foreach($ad->media_img as $img)
                                            <img src="{{ media_url($img, true) }}" alt="{{ $ad->title }}">
                                        @endforeach
                                    </div>
                                </div>

                            @endif

                        @if($enable_monetize)
                            {!! get_option('monetize_code_below_ad_image') !!}
                        @endif
                    </div>

                    <div class="col-sm-5 col-xs-12">
                        <h2 class="ad-title"><a href="{{  route('single_ad', [$ad->id, $ad->slug]) }}">{{ $ad->title }}</a>  </h2>
                        <div class="ads-detail-meta">
                            <p class="text-muted">
                                @if($ad->category)
                                <i class="fa fa-folder-o"></i><a href="{{ route('listing', ['category' => $ad->category->id]) }}">  {{ $ad->category->category_name }} </a> |
                                @endif

                                @if($ad->brand)
                                    <i class="fa fa-industry"></i><a href="{{ route('listing', ['brand' => $ad->brand->id]) }}">  {{ $ad->brand->brand_name }} </a> |
                                @endif
                            </p>
                        </div>

                        <h2 class="modern-single-ad-price">{{ themeqx_price_ng($ad->price) }}</h2>

                        @if($enable_monetize)
                            {!! get_option('monetize_code_below_ad_title') !!}
                        @endif


                        @if($enable_monetize)
                            {!! get_option('monetize_code_above_general_info') !!}
                        @endif

                        <h3>@lang('app.general_info')</h3>
                        <p><strong><i class="fa fa-money"></i> @lang('app.price')</strong> {{ themeqx_price_ng($ad->price) }} </p>
                        <p><strong><i class="fa fa-map-marker"></i>  @lang('app.location') </strong> {!! $ad->full_address() !!} </p>
                        <p><strong><i class="fa fa-check-circle-o"></i> @lang('app.condition')</strong> {{ $ad->ad_condition }} </p>

                        @if($enable_monetize)
                            {!! get_option('monetize_code_below_general_info') !!}
                        @endif

                        <div class="modern-social-share-btn-group">
                            <h4>@lang('app.share_this_ad')</h4>
                            <a href="#" class="btn btn-default share s_facebook"><i class="fa fa-facebook"></i> </a>
                            <a href="#" class="btn btn-default share s_plus"><i class="fa fa-google-plus"></i> </a>
                            <a href="#" class="btn btn-default share s_twitter"><i class="fa fa-twitter"></i> </a>
                            <a href="#" class="btn btn-default share s_linkedin"><i class="fa fa-linkedin"></i> </a>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>