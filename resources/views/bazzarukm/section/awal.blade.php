

                <div class="row row-explore-nerby bgf3 p-bottom60">
                    <div class="m1170">
                        <div class="col-sm-12">
                            <div class="about-title extrabold uppercase color333 top60 btm15">
                                <span class="bgf3 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Produk UKM Rekomendasi Kami | </span>
                            </div>
                            <div class="font14 color777 text-center wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Find out & explore new businesses near your area and see what's happening !!!</div>

                            <div id="myCarousel2" class="carousel slide blog-reviews" data-interval="3000">
                                <ol class="carousel-indicators blog-reviews-pagination">
                                    <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel2" data-slide-to="1"></li>
                                    <li data-target="#myCarousel2" data-slide-to="2"></li>
                                </ol>   
                                <div class="carousel-inner">
                                    <div class="active item">
                                        <div class="members-acc">
                                            @foreach($premium_ads as $ad)
                                            <div class="col-sm-3 top60 f-left">
                                                <div class="member-prof " data-wow-delay="0.7s" data-wow-duration="1.5s">
                                                    <div class="memb-photo memb-photo1">
                                                        <img src="{{ media_url($ad->feature_img) }}" alt="wd">
                                                        <div class="memb-photo-hover memb-photo-hover1">
                                                            <div class="memb-photo-links">
                                                                <a href="#">
                                                                    <div class="img-icons"><i class="fa fa-heart-o">&nbsp;</i></div>
                                                                </a>
                                                                <a href="#">
                                                                    <div class="img-icons"><i class="fa fa-share-alt">&nbsp;</i></div>
                                                                </a><br />
                                                                <ul class="list-styles raitings-stars p-left10 inl-flex">
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="memb-txt">
                                                        <span class="font11 semibold uppercase blue">beauty & spa</span><br />
                                                        <b class="font14 color333 uppercase">Amicus Hair and Beauty</b><br />
                                                        <span class="font12 color777">5108 Macarthur Blvd NW Unit B, Washington, DC 20016</span>
                                                        <div class="font12 color777 top10"><i class="fa fa-phone"></i> <b>(202) 364-6772</b></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            
                                             
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                 
