@extends('bazzarukm.layouts.main')
@section('title') Halaman Pendaftaran | @parent @endsection

@section('main')
<section style="background:#f8f8f8">
    <!-- banner end -->
    <div class="container-fluid">
        <div class="row margin0">
            <div class="m1170">
               <div class="col-sm-8 col-sm-offset-2 top40">
                <center><img src="{{ asset('assets/tema/bazzarukm/img/customer-service.svg') }}" style="max-height:140px;padding: 0 20px 0 20px">
                </center>
                
                <div class="about-title extrabold uppercase color333 width100"> 
                    <span class="bgfff wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Form Pendaftaran</span>
                </div>
            <div class="panel panel-default">
                <div class="panel-heading"> 
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <!-- note pendaftaran -->
    <div class="col-sm-8 col-sm-offset-2 top40" style="background:#f8f8f8">
        <center><img src="{{ asset('assets/tema/bazzarukm/img/online-shop.svg') }}" style="max-height:80px;padding: 0 20px 0 20px">
                </center>
          <div class="about-title extrabold uppercase color333 width100">
            <span class="bgfff wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Catatan</span>
        </div>
                            <div class="col-sm-12 row-sm-offset-3 socialButtons">
 <ul>
Dengan password yang Anda miliki, Anda bisa mengakses Iklan Produk, sehingga Anda bisa:

   <li> Edit atau hapus iklan Anda</li>
    <li>Melihat tanggapan terhadap iklan produk Anda</li>
 
<li>Masukkan alamat e-mail dan password, kami akan mengirimkan email konfirmasi.</li>
</ul>                            </div>
    </div>

</div>

</div>
</section><div class="clearfix top40"  ></div>

@endsection
