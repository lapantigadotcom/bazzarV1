<script type="text/javascript">
function htmlbodyHeightUpdate() {
    var height3 = $(window).height();
    var height1 = $('.nav').height() + 50;
    height2 = $('.container-main').height();
    if (height2 > height3) {
        $('html').height(Math.max(height1, height3, height2) + 10);
        $('body').height(Math.max(height1, height3, height2) + 10);
    } else
    {
        $('html').height(Math.max(height1, height3, height2));
        $('body').height(Math.max(height1, height3, height2));
    }

}
$(document).ready(function () {
    htmlbodyHeightUpdate();
    $(window).resize(function () {
        htmlbodyHeightUpdate();
    });
    $(window).scroll(function () {
        height2 = $('.container-main').height();
        htmlbodyHeightUpdate();
    });
});</script>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/tema/bazzarukm/css/dashboard.css') }}">

<nav class="navbar navbar-m2p sidebar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
             
         </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                 
                <!-- Banner -->
                <li class="">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-list"></i> @lang('app.my_ads') <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                      <li>  <a href="{{ route('my_ads') }}"><i class="fa fa-dot-circle-o"></i> @lang('app.my_ads')</a> </li>
                      <li>  <a href="{{ route('create_ad') }}"><i class="fa fa-dot-circle-o"></i> @lang('app.post_an_ad')</a> </li>
                      <li>  <a href="{{ route('pending_ads') }}"><i class="fa fa-dot-circle-o"></i> @lang('app.pending_for_approval')</a> </li>
                      <li>  <a href="{{ route('favorite_ads') }}"><i class="fa fa-dot-circle-o"></i> @lang('app.favourite_ads')</a> </li>
                  </ul>
              </li>
              @if($lUser->is_admin())
 
              <!-- iklan -->
              <li> <a href="{{ route('parent_categories') }}"><i class="fa fa-list"></i> @lang('app.categories')</a>  </li>
              <li> <a href="{{ route('admin_brands') }}"><i class="fa fa-adjust"></i> @lang('app.brands')</a>  </li>
              <li class="">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-list"></i> @lang('app.ads') <span class="caret"></span>
                </a>
                <ul class="dropdown-menu forAnimate" role="menu">
                   <li>  <a href="{{ route('approved_ads') }}">@lang('app.approved_ads')</a> </li>
                   <li>  <a href="{{ route('admin_pending_ads') }}">@lang('app.pending_for_approval')</a> </li>
                   <li>  <a href="{{ route('admin_blocked_ads') }}">@lang('app.blocked_ads')</a> </li>
               </ul>
           </li>

           <!-- Blog -->
           <li class="">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 @lang('app.blog') <span class="caret"></span>
            </a>
            <ul class="dropdown-menu forAnimate" role="menu">
               <li>  <a href="{{ route('posts') }}">@lang('app.posts')</a> </li>
               <li>  <a href="{{ route('create_new_post') }}">@lang('app.create_new_post')</a> </li>
           </ul>
               </li> 
               <li> <a href="{{ route('pages') }}"><i class="fa fa-file-word-o"></i> @lang('app.pages')</a>  </li>
               <li> <a href="{{ route('slider') }}"><i class="fa fa-sliders"></i> @lang('app.slider')</a>  </li>
               <li> <a href="{{ route('ad_reports') }}"><i class="fa fa-exclamation"></i> @lang('app.ad_reports')</a>  </li>
               <li> <a href="{{ route('users') }}"><i class="fa fa-users"></i> @lang('app.users')</a>  </li>
               <!-- tema -->
               <li class="">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     @lang('app.appearance')<span class="caret"></span>
                </a>
                <ul class="dropdown-menu forAnimate" role="menu">
                    <li> <a href="{{ route('theme_settings') }}">@lang('app.theme_settings')</a> </li>
                    <li> <a href="{{ route('modern_theme_settings') }}">@lang('app.modern_theme_settings')</a> </li>
                    <li> <a href="{{ route('social_url_settings') }}">@lang('app.social_url')</a> </li>
                </ul>
            </li> 
    <!-- lokasi -->
            <li class="">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     @lang('app.locations')<span class="caret"></span>
                </a>
                <ul class="dropdown-menu forAnimate" role="menu">
                    <li> <a href="{{ route('country_list') }}">@lang('app.countries')</a> </li>
                    <li> <a href="{{ route('state_list') }}">@lang('app.states')</a> </li>
                    <li> <a href="{{ route('city_list') }}">@lang('app.cities')</a> </li>
                </ul>
            </li> 
            <li> <a href="{{ route('contact_messages') }}"><i class="fa fa-envelope-o"></i> @lang('app.contact_messages')</a>  </li>
            <!-- seting -->
            <li class="">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     @lang('app.settings')<span class="caret"></span>
                </a>
                <ul class="dropdown-menu forAnimate" role="menu">
                                <li> <a href="{{ route('monetization') }}"><i class="fa fa-dollar"></i> @lang('app.monetization')</a>  </li>
                                <li> <a href="{{ route('administrators') }}"><i class="fa fa-users"></i> @lang('app.administrators')</a>  </li>
                   <li> <a href="{{ route('general_settings') }}">@lang('app.general_settings')</a> </li>
                    <li> <a href="{{ route('ad_settings') }}">@lang('app.ad_settings_and_pricing')</a> </li>
                    <li> <a href="{{ route('payment_settings') }}">@lang('app.payment_settings')</a> </li>
                    <li> <a href="{{ route('language_settings') }}">@lang('app.language_settings')</a> </li>
                    <li> <a href="{{ route('file_storage_settings') }}">@lang('app.file_storage_settings')</a> </li>
                    <li> <a href="{{ route('social_settings') }}">@lang('app.social_settings')</a> </li>
                    <li> <a href="{{ route('blog_settings') }}">@lang('app.blog_settings')</a> </li>
                    <li> <a href="{{ route('other_settings') }}">@lang('app.other_settings')</a> </li>
                </ul>
            </li> 
             
                @endif
                 <li> <a href="{{ route('payments') }}"><i class="fa fa-money"></i> @lang('app.payments')</a>  </li>
            <li> <a href="{{ route('profile') }}"><i class="fa fa-user"></i> @lang('app.profile')</a>  </li>
            <li> <a href="{{ route('change_password') }}"><i class="fa fa-lock"></i> @lang('app.change_password')</a>  </li>

            </ul>
        </div>
    </div>
</nav>