@extends('bazzarukm.layouts.main')

@section('main')
<section>
   
<div class="container-fluid">
<div class="row new-details ">
    <div class="m1170 ">
        <!-- main dashboard -->
        <div class="col-sm-12 top10">
              @if(session('error'))
                         <div class="row">
                            <div class="col-lg-12">
                                <br />
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <center><h3 class="panel-title">
                                    <span class="glyphicon glyphicon-dash"></span><i class="fa fa-gear"></i> @lang('app.dashboard')
                                </h3></center>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-light-green btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-clock-o fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $approved_ads }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.approved_ads')</button>
                                        </a>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-info btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-desktop fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $pending_ads }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.pending_ads')</button>
                                        </a>
                                    </div>
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-danger btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-desktop fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $blocked_ads }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.blocked_ads')</button>
                                        </a>
                                    </div>

                                    @if($ten_contact_messages)
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-warning btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-shopping-cart fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $total_users }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.users')</button>
                                        </a>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-purple btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-print fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $total_payments }}</div>
                                            <button class="btn btn-default  btn-block">@lang('app.success_payments')</button>
                                        </a>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-deep-purple btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-suitcase fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $total_payments_amount }} <sup>{{ get_option('currency_sign') }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.total_payment')</button>
                                        </a>
                                    </div>
                                    @endif
                                    <div class="col-lg-2">
                                        <a href="#" class="btn btn-pink btn-lg btn-block dash-widget" role="button" style="padding:2px;">
                                            <div id="box_1"><span class="fa fa-user fa-3x"></span></div>
                                            <div id="box_2" class="icon-label">{{ $total_reports }}</div>
                                            <button class="btn btn-default btn-block">@lang('app.reports')</button>
                                        </a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div></div>
            <!-- /.icon atas -->
        <div class="col-sm-9 top10">
        
                @if($lUser->is_admin())
            <div class="row">
                @if($ten_contact_messages)
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @lang('app.latest_ten_contact_messages')
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>@lang('app.sender')</th>
                                    <th>@lang('app.message')</th>
                                </tr>

                                @foreach($ten_contact_messages as $message)
                                    <tr>
                                        <td>
                                            <i class="fa fa-user"></i> {{ $message->name }} <br />
                                            <i class="fa fa-envelope-o"></i> {{ $message->email }} <br />
                                            <i class="fa fa-clock-o"></i> {{ $message->created_at->diffForHumans() }}
                                        </td>
                                        <td>{{ $message->message }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                @if($reports)
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @lang('app.latest_ten_ads_report')
                        </div>
                        <div class="panel-body">

                            @if($reports->count() > 0)
                                <table class="table table-bordered table-striped table-responsive">
                                    <tr>
                                        <th>@lang('app.reason')</th>
                                        <th>@lang('app.email')</th>
                                        <th>@lang('app.message')</th>
                                        <th>@lang('app.ad_info')</th>
                                    </tr>

                                    @foreach($reports as $report)
                                        <tr>
                                            <td>{{ $report->reason }}</td>
                                            <td> {{ $report->email }}  </td>
                                            <td>
                                                {{ $report->message }}
                                                <hr />
                                                <p class="text-muted"> <i>@lang('app.date_time'): {{ $report->posting_datetime() }}</i></p>
                                            </td>
                                            <td>
                                                @if($report->ad)
                                                    <a href="{{ route('single_ad', [$report->ad->id, $report->ad->slug]) }}" target="_blank">@lang('app.view_ad')</a>
                                                    <i class="clearfix"></i>
                                                    <a href="{{ route('reports_by_ads', $report->ad->slug) }}">
                                                        <i class="fa fa-exclamation-triangle"></i> @lang('app.reports') : {{ $report->ad->reports->count() }}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif

                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
       

        </div> 
        <!-- sidebar dashboard -->
        <div class="col-sm-3 profile">
         @include('admin.sidebar_menu')
        </div> 

    </div> 
</div>
</div> 
</section>            
@endsection