@extends('bazzarukm.layouts.main')

@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection


@section('main')
<section>
<div class="container-fluid">
<div class="row new-details ">
    <div class="m1170 ">
        <!-- main dashboard -->
        @include('admin.flash_msg')
        <div class="col-sm-12 top10">


             <div class="col-sm-9 top10">
                <!-- atas 1 -->
               <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#katogoribaru">
                                <i class="fa fa-envelope-o"> </i> + Tambahkan Kategori
                            </button>
                                    @include('admin.modal_kategori')

                    <hr>
                    <!-- kategori list -->
                     <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered">
                            <tr>
                                <th>@lang('app.category_name') (@lang('app.total_products')) </th>
                            </tr>
                            @foreach($categories as $category)
                                <tr class="col-sm-4 top40">
                                    <td>
                                        <div class="clearfix"> 
                                            <strong title="{{$category->category_name}}">{!! strip_tags(substr($category->category_name,0,15)).'...' !!}({{ $category->product_count }})</strong>
                                            <span class="pull-right">

                                           &nbsp; &nbsp;&nbsp;<a href="{{ route('edit_categories', $category->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                                            <a href="javascript:;" class="btn btn-danger btn-xs" data-id="{{ $category->id }}"><i class="fa fa-trash"></i> </a>
                                            </span>

                                        </div>

                                        @if($category->sub_categories->count() > 0)
                                            @foreach($category->sub_categories as $sub_cat)
                                                <div class="clearfix">
                                                    <hr style="margin: 3px 0" />

                                                    <i class="fa fa-dot-circle-o"></i>  <t title="{{$sub_cat->category_name}}">{!! strip_tags(substr($sub_cat->category_name,0,15)).'...' !!} ({{ $category->product_count }}) </t>

                                                    <span class="pull-right">
                                                        <a href="{{ route('edit_categories', $sub_cat->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                                                        <a href="javascript:;" class="btn btn-danger btn-xs" data-id="{{ $sub_cat->id }}"><i class="fa fa-trash"></i> </a>
                                                    </span>

                                                </div>

                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

             </div>

            <div class="col-sm-2 profile">
                @include('admin.sidebar_menu')
            </div> 

        </div>



    </div>
</div>
</div>

</section>

@endsection

@section('page-js')
    <script>
        $(document).ready(function() {
            $('.btn-danger').on('click', function (e) {
                if (!confirm("Are you sure? its can't be undone")) {
                    e.preventDefault();
                    return false;
                }

                var selector = $(this);
                var data_id = $(this).data('id');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('delete_categories') }}',
                    data: {data_id: data_id, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('div').hide('slow');
                        }
                    }
                });
            });
        });
    </script>
@endsection