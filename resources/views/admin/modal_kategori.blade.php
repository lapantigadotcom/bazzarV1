<div class="modal fade" id="katogoribaru" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambahkan Kategori Baru</h4>
                </div>
                <div class="modal-body">

                      <div class="row">
                        <div class="col-sm-12">
                        {{ Form::open(['class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label for="category_name" class="col-sm-4 control-label">@lang('app.select_a_category')</label>

                            <div class="col-sm-12">
                                <select class="form-control select2" name="parent_category">
                                    <option value="0">@lang('app.top_category')</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('category_name')? 'has-error':'' }}">
                            <label for="category_name" class="col-sm-4 control-label">@lang('app.category_name')</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="category_name" value="{{ old('category_name') }}" name="category_name" placeholder="@lang('app.category_name')">
                                {!! $errors->has('category_name')? '<p class="help-block">'.$errors->first('category_name').'</p>':'' !!}

                            </div>
                        </div>



                        <div class="form-group">
                            <label for="fa_icon" class="col-sm-4 control-label">@lang('app.select_icon')</label>
                            <div class="col-sm-12">
                                <select class="form-control select2icon" name="fa_icon">
                                    <option value="0">@lang('app.select_icon')</option>
                                    @foreach(fa_icons() as $icon => $val)
                                        <option value="{{ $icon }}" data-icon="{{ $icon }}">{{ $icon }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="`color_class" class="col-sm-4 control-label">@lang('app.select_color')</label>
                            <div class="col-sm-12">
                                <select class="form-control select2" name="color_class">
                                    @foreach(category_classes() as $class)
                                        <option value="{{ $class }}">{{ $class }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                            <label for="description" class="col-sm-4 control-label">@lang('app.description')</label>
                            <div class="col-sm-12">
                                <textarea name="description" id="description" class="form-control" rows="6">{{ old('description') }}</textarea>
                                {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-12">
                                <button type="submit" class="btn btn-primary">@lang('app.save_new_category')</button>
                            </div>
                        </div>
                        {{ Form::close() }}

                </div>  </div>

                </div>

            </div>
        </div>
</div>