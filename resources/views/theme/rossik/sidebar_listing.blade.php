            <div class="col-sm-3 top10">

                <div class="bg-white">
                    <div class="sidebar-filter-wrapper">

                        @if($enable_monetize)
                        {!! get_option('monetize_code_listing_sidebar_top') !!}
                        @endif

                        {{ Form::open([ 'method'=>'get', 'id' => 'listingFilterForm']) }}

                        <div class="row">
                            <div class="col-xs-12">
                                <p class="listingSidebarLeftHeader">@lang('app.filter_ads')
                                    <span id="loaderListingIcon" class="pull-right" style="display: none;"><i class="fa fa-spinner fa-spin"></i></span>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" />
                        </div>

                        <hr />
                        <div class="form-group">
                            <select class="form-control select2" name="category">
                                <option value="">@lang('app.select_a_category')</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ request('category') ==  $category->id ? 'selected':'' }}>{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" id="sub_category_select" name="sub_category">
                                <option value="">@lang('app.select_a_sub_category')</option>
                                @if($selected_categories)
                                @foreach($selected_categories->sub_categories as $sub_category)
                                <option value="{{ $sub_category->id }}" {{ request('sub_category') ==  $sub_category->id ? 'selected':'' }} >{{ $sub_category->category_name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" name="brand" id="brand_select">
                                <option value=""> @lang('app.select_a_brand') </option>
                                @if($selected_sub_categories)
                                @foreach($selected_sub_categories->brands as $brand)
                                <option value="{{ $brand->id }}" {{ request('brand') ==  $brand->id ? 'selected':'' }} >{{ $brand->brand_name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <hr />

                        <!-- @php $country_usage = get_option('countries_usage'); @endphp
                        @if($country_usage == 'all_countries')
                        <div class="form-group">
                            <select class="form-control select2" name="country">
                                <option value="">@lang('app.select_a_country')</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ request('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif -->

                        <div class="form-group">
                            <select class="form-control select2" id="state_select" name="state">
                                <option value=""> @lang('app.select_state') </option>
                                @if($selected_countries)
                                @foreach($selected_countries->states as $state)
                                <option value="{{ $state->id }}" {{ request('state') ==  $state->id ? 'selected':'' }} >{{ $state->state_name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" id="city_select" name="city">
                                <option value=""> @lang('app.select_city') </option>
                                @if($selected_states)
                                @foreach($selected_states->cities as $city)
                                <option value="{{ $city->id }}" {{ request('city') ==  $city->id ? 'selected':'' }} >{{ $city->city_name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label>@lang('app.price_min_max')</label>

                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="number" class="form-control" name="min_price" value="{{ request('min_price') }}" placeholder="@lang('app.min_price')" />
                                </div>
                                <div class="col-xs-6">
                                    <input type="number" class="form-control" name="max_price" value="{{ request('max_price') }}" placeholder="@lang('app.max_price')" />
                                </div>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label>@lang('app.condition')</label>
                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="condition" id="new" value="new" {{ request('condition') == 'new'? 'checked':'' }}>
                                    @lang('app.new')
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="condition" id="used" value="used" {{ request('condition') == 'used'? 'checked':'' }}>
                                    @lang('app.used')
                                </label>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <div class="row">
                                <div class=" col-sm-6 col-xs-12">
                                    <button class="btn btn-primary btn-block"><i class="fa fa-search"></i>  @lang('app.filter')</button>
                                </div>
                                <div class=" col-sm-6 col-xs-12">
                                    <a href="{{ route('listing') }}" class="btn btn-default btn-block"><i class="fa fa-refresh"></i>  @lang('app.reset')</a>
                                </div>
                            </div>
                        </div>

                        {{ Form::close() }}
                        <div class="clearfix"></div>

                        @if($enable_monetize)
                        {!! get_option('monetize_code_listing_sidebar_bottom') !!}
                        @endif

                    </div>

                </div>
            </div>