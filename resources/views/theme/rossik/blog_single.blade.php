@extends('bazzarukm.layouts.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection


@section('social-meta')
    <meta property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ substr(trim(preg_replace('/\s\s+/', ' ',strip_tags($post->post_content) )),0,160) }}">
    @if($post->feature_img)
        <meta property="og:image" content="{{ media_url($post->feature_img, true) }}">
    @else
        <meta property="og:image" content="{{ asset('uploads/placeholder.png') }}">
    @endif
    <meta property="og:url" content="{{ route('blog_single', $post->slug) }}">
    <meta name="twitter:card" content="summary_large_image">
    <!--  Non-Essential, But Recommended -->
    <meta name="og:site_name" content="{{ get_option('site_name') }}">
@endsection

@section('main')
 
<section> 
            <div class="container-fluid">
                <div class="row new-details">
                    <div class="m1170">
                        <div class="col-sm-9 top80">
                            <article> 
                                <div class="first-post">
                                    <div class="first-pict wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                     @if($post->feature_img)
                                     <img class="img-responsive" alt="{{ $post->title }}" src="{{ media_url($post->feature_img) }}">
                                     @else
                                     <img class="img-responsive" alt="{{ $post->title }}" src="{{ asset('uploads/placeholder.png') }}">
                                     @endif
                                        <div class="pict-data uppercase"><i class="fa fa-clock-o">&nbsp;</i>{{ $post->created_at_datetime() }}</div>
                                    </div>
                                    <div class="font22 color333 extrabold uppercase top30 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">amet consectetur adipisicing elit</div>
                                    <div class="f-left p-right20 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                        <ul class="list-styles new-first-det start0 f-left top10">
                                         @if($post->author)
                                         <li><a href="{{ route('author_blog_posts', $post->author->id) }}"><i class="fa fa-user">&nbsp;</i>{{ $post->author->name }}</a></li>
                                         @endif
                                        </ul>
                                    </div>
                                    <div class="f-left wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                        <ul class="list-styles new-first-det start0 f-left top10">
                                            <li><a href="#"><i class="fa fa-folder-open-o">&nbsp;</i>FOOD</a></li>
                                            <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="s-row-line margin20 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                    </div>
                                    <div class="color777 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                        {!! $post->post_content !!}
                                    </div>
                                     
                                </div>          
                            </article>
                             
                            <div class="about-title extrabold uppercase top60">
                                <span class="bgfff font22 color333 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Komentar</span>
                            </div>
                            <div class="blog-det-comments">
                                <div class="blog-det-comment top40 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                 @if($enable_discuss)
                                 <div class="comments-title"><h2> <i class="fa fa-comment"></i> @lang('app.comments')</h2></div>

                                 <div id="disqus_thread"></div>
                                 <script>
                                 var disqus_config = function () {
                            this.page.url = '{{ route('blog_single', $post->slug) }}';  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = '{{ route('blog_single', $post->slug) }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };
                        (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = '//{{get_option('disqus_shortname')}}.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                        @endif
                                </div>
                                
                            </div>
                                       
                            <div class="clearfix"></div>                
                        </div>

                        <div class="col-sm-3 top80">
                            <div class="search width100 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                <input class="search-input width100 f-left borderccc" placeholder="Search Blog">
                                <a href="#"><div class="search-button f-right"><i class="fa fa-search"></i></div></a>
                            </div>
                            <nav>
                                <div class="nav-right-menu">
                                    <div class="right-title uppercase"><i class="fa fa-star"></i>categories</div>
                                    <ul class="start0">
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Directory Listings</li>
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Food & Drink</li>
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Auto Dealers</li>
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Shipping Companies</li>
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Creative Agency</li>
                                        <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Lawyers</li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </nav>
                            <div class="recent-coupons">
                                <div class="right-title uppercase wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><i class="fa fa-star"></i>recent coupons</div>
                                <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                    <div class="coupon-img">
                                        <img src="media/262x150/coupon1.jpg" alt="wd">
                                        <div class="off">50% OFF</div>
                                    </div>
                                    <a href="#"><div class="font14 color333 uppercase top10"><b>consecte tur dolore</b></div></a>
                                    <div>Eiusmod tempor incidiunt labore velit dol
                                        emagna aliqu sedu enimi...
                                    </div>
                                    <ul class="list-styles new-first-det start0 f-left">
                                        <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                                        <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
                                    </ul><div class="clearfix"></div>
                                </div>
                                <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                    <div class="coupon-img">
                                        <img src="media/262x150/coupon2.jpg" alt="wd">
                                        <div class="off">20% OFF</div>
                                    </div>
                                    <a href="#"><div class="font14 color333 uppercase top10"><b>consecte tur dolore magna</b></div></a>
                                    <div>Eiusmod tempor incidiunt labore velit dol
                                        emagna aliqu sedu enimi...
                                    </div>
                                    <ul class="list-styles new-first-det start0 f-left">
                                        <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                                        <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
                                    </ul><div class="clearfix"></div>
                                </div>
                                <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                                    <div class="coupon-img">
                                        <img src="media/262x150/coupon3.jpg" alt="wd">
                                        <div class="off">20% OFF</div>
                                    </div>
                                    <a href="#"><div class="font14 color333 uppercase top10"><b>tur dolore magna</b></div></a>
                                    <div>Eiusmod tempor incidiunt labore velit dol
                                        emagna aliqu sedu enimi...
                                    </div>
                                    <ul class="list-styles new-first-det start0 f-left">
                                        <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                                        <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
                                    </ul><div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="latest-tags">
                                <div class="right-title uppercase"><i class="fa fa-star"></i>latest tags</div>
                                <div class="blog-news-nav top20">
                                    <a href="04-category-2.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Latest Offers</div>
                                    </a>
                                    <a href="04-category-2.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Responsive</div>
                                    </a>
                                    <a href="04-category-2.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">recent listings</div>
                                    </a>
                                    <a href="03-category-1.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">restaurant</div>
                                    </a>
                                    <a href="03-category-1.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Themeforest</div>
                                    </a>
                                    <a href="03-category-1.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Web Design</div>
                                    </a>
                                    <a href="05-category-3.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">taxi hiring</div>
                                    </a>
                                    <a href="05-category-3.html">
                                        <div class="blog-nav-el uppercase border1 borderccc color777 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Interior Painting</div>
                                    </a>
                                </div>
                            </div>

                        </div>              

                    </div>
                </div>
            </div><div class="clearfix"></div>
        </section>
@endsection

@section('page-js')
    @if($enable_discuss)
        <script id="dsq-count-scr" src="//tclassifieds.disqus.com/count.js" async></script>
    @endif
    <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
        @endif
    </script>
@endsection