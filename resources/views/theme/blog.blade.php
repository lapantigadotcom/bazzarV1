@extends('bazzarukm.layouts.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('main')
<section>
 <div class="container-fluid">
    <div class="row new-details">
        <div class="m1170">
            <!-- MAIN CONTENT -->
            <div class="col-sm-9 top40">

             <article clas=""> @foreach($posts as $post)
                <div class="first-post">
                    <div class="first-pict wow fadeInUp top40" data-wow-delay="0.7s" data-wow-duration="1.5s">
                       @if($post->feature_img)
                       <img class="img-responsive" alt="{{ $post->title }}" src="{{ media_url($post->feature_img) }}">
                       @else
                       <img class="img-responsive" alt="{{ $post->title }}" src="{{ asset('uploads/placeholder.png') }}">
                       @endif
                       <div class="pict-data uppercase"><i class="fa fa-clock-o">&nbsp;</i>{{ $post->created_at_datetime() }}</div>
                   </div>
                   <div class="font22 color333 extrabold uppercase top30 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><a href="{{ route('blog_single', $post->slug) }}">{{ $post->title }}</a></div>
                   <div class="f-left p-right20 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                    <ul class="list-styles new-first-det start0 f-left top10">
                     @if($post->author)
                     <li><a href="{{ route('author_blog_posts', $post->author->id) }}"><i class="fa fa-user">&nbsp;</i>{{ $post->author->name }}</a></li>
                     @endif
                 </ul>
             </div>
             <div class="f-left wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                <ul class="list-styles new-first-det start0 f-left top10">
                    <li><a href="#"><i class="fa fa-folder-open-o">&nbsp;</i>FOOD</a></li>
                    <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div class="color777 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
                {{ str_limit(strip_tags($post->post_content), 250) }}
            </div>

        </div>@endforeach       
    </article>
    
  </div>
 <!-- SIDEBAR -->
<div class="col-sm-3 top80">
    <div class="search width100 wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
        <input class="search-input width100 f-left borderccc" placeholder="Search Blog">
        <a href="#"><div class="search-button f-right"><i class="fa fa-search"></i></div></a>
    </div>
    <nav>
        <div class="nav-right-menu">
            <div class="right-title uppercase"><i class="fa fa-star"></i>categories</div>
            <ul class="start0">
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Directory Listings</li>
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Food & Drink</li>
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Auto Dealers</li>
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Shipping Companies</li>
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Creative Agency</li>
                <li class="menu-link wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Lawyers</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </nav>
    <div class="recent-coupons">
        <div class="right-title uppercase wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><i class="fa fa-star"></i>recent coupons</div>
        <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
            <div class="coupon-img">
                <img src="media/262x150/coupon1.jpg" alt="wd">
                <div class="off">50% OFF</div>
            </div>
            <a href="#"><div class="font14 color333 uppercase top10"><b>consecte tur dolore</b></div></a>
            <div>Eiusmod tempor incidiunt labore velit dol
                emagna aliqu sedu enimi...
            </div>
            <ul class="list-styles new-first-det start0 f-left">
                <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
            </ul><div class="clearfix"></div>
        </div>
        <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
            <div class="coupon-img">
                <img src="media/262x150/coupon2.jpg" alt="wd">
                <div class="off">20% OFF</div>
            </div>
            <a href="#"><div class="font14 color333 uppercase top10"><b>consecte tur dolore magna</b></div></a>
            <div>Eiusmod tempor incidiunt labore velit dol
                emagna aliqu sedu enimi...
            </div>
            <ul class="list-styles new-first-det start0 f-left">
                <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
            </ul><div class="clearfix"></div>
        </div>
        <div class="coupon wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
            <div class="coupon-img">
                <img src="media/262x150/coupon3.jpg" alt="wd">
                <div class="off">20% OFF</div>
            </div>
            <a href="#"><div class="font14 color333 uppercase top10"><b>tur dolore magna</b></div></a>
            <div>Eiusmod tempor incidiunt labore velit dol
                emagna aliqu sedu enimi...
            </div>
            <ul class="list-styles new-first-det start0 f-left">
                <li><a href="#"><i class="fa fa-heart-o">&nbsp;</i>30 Likes</a></li>
                <li><a href="#"><i class="fa fa-share-alt">&nbsp;</i>share</a></li>
            </ul><div class="clearfix"></div>
        </div>
    </div>

    <div class="latest-tags">
        <div class="right-title uppercase"><i class="fa fa-star"></i>latest tags</div>
        <div class="blog-news-nav top20">
            <a href="04-category-2.html">
                <div class="blog-nav-el uppercase border1 borderccc color777 right7 wow zoomIn" data-wow-delay="0.7s" data-wow-duration="1.5s">Latest Offers</div>
            </a> 
        </div>
    </div>

</div>              

</div>
</div>
</div><div class="bottom40"></div>
</section>
@endsection

@section('page-js')
<script>
@if(session('success'))
toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
@endif
</script>
@endsection