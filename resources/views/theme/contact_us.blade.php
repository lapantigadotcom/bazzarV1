@extends('bazzarukm.layouts.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('main')

    <div class="jumbotron jumbotron-xs">
        <div class="container">
            <div class="row m1170">
                <div class="col-sm-12 col-lg-12 top40">
                      <div class="about-title extrabold uppercase color333 width100">
                    <span class="bgfff wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">@lang('app.contact_with_us')</span>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container bottom50">
        <div class="row">
            <div class="col-md-4">
                 <form>
                    <legend><span class="glyphicon glyphicon-globe"></span> Bazzarukm.com</legend>

                    <address>
                        <strong>{{ get_text_tpl(get_option('footer_company_name')) }}</strong>
                        @if(get_option('footer_address'))
                            <br />
                            <i class="fa fa-map-marker"></i>
                            {!! get_option('footer_address') !!}
                        @endif
                        @if(get_option('site_phone_number'))
                            <br><i class="fa fa-phone"></i>
                            <abbr title="Phone">{!! get_option('site_phone_number') !!}</abbr>
                        @endif

                    </address>

                    @if(get_option('site_email_address'))
                        <address>
                            <strong>@lang('app.email')</strong>
                            <br> <i class="fa fa-envelope-o"></i>
                            <a href="mailto:{{ get_option('site_email_address') }}"> {{ get_option('site_email_address') }} </a>
                        </address>
                    @endif

                </form>
            </div>
            <div class="col-md-7">

                   <div class=" ">
                {!! Form::open() !!}
                <div class="row">
                    <div class="col-md-12 el-form-2">
                        <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                            <label for="name">Nama : </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap Anda ..." value="{{ old('name') }}" required="required" />
                             </div>
                             {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                        </div>
                        <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                            <label for="email">@lang('app.email_address') : </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" placeholder="Email aktif Anda ..." name="email" value="{{ old('email') }}" required="required" />
                            </div>
                            {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}

                        </div>

                        <div class="form-group {{ $errors->has('message')? 'has-error':'' }}">
                            <label for="name">Pesan, Pertanyaan, Saran : </label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-envelope"></span>
                                </span>
                            
                            <textarea name="message" id="message" class="form-control" required="required" placeholder="@lang('app.message')">{{ old('message') }}</textarea>
                            </div> {!! $errors->has('message')? '<p class="help-block">'.$errors->first('message').'</p>':'' !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="cart-button" style="color:#fff" style="text-align:left" id="btnContactUs"> <i class="fa fa-paper-plane"></i> Kirim</button>
                    </div>
                </div>
            </form>
            </div>
                            </div>

           
        </div>
    </div>



@endsection

@section('page-js')


    <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
        @endif
    </script>
@endsection