@extends('bazzarukm.layouts.main')
@section('title') Halaman Login | @parent @endsection
@section('main')
<section style="background:#f8f8f8">
    <!-- banner end -->
    <div class="container-fluid">
        <div class="row margin0">
            <div class="m1170">
                <div class="main-content-contucts">
                    <div class="col-sm-6 col-sm-offset-3 top40">
                        <center><img src="{{ asset('assets/tema/bazzarukm/img/competition.svg') }}" style="max-height:140px;padding: 0 20px 0 20px">
                        </center>
                        <div class="about-title extrabold uppercase color333 width100">
                            <span class="bgfff wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Silahkan Login</span>
                        </div>
                        @if(session('registration_success'))
                        <div id="success">{{ session('registration_success') }}</div>
                        @endif                                

                        {{ Form::open(['class'=> 'loginForm', 'autocomplete'=> 'off']) }}
                        <br>
                        <div class="col-sm-12 el-form-2" id="el-form-2">
                            <div class="input-group {{ $errors->has('email')? 'has-error':'' }}">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                            </div>
                            {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                            <span class="help-block"></span>

                            <div class="input-group {{ $errors->has('password')? 'has-error':'' }}">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input  type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            {!! $errors->has('password')? '<p class="help-block">'.$errors->first('password').'</p>':'' !!}

                            <span class="help-block"></span>
                        </div>  
                        <div class="input-group col-sm-12 top20">
                            <input type="submit" value="SUBMIT" class="submit-btn button1-1 b-radius3 right30 button-blue top20">
                            <div class="checkbox checkbox-2 f-left top30">
                                <input id="check5" type="checkbox" name="check" value="check5" checked>
                                <label for="check5">Ingat Saya</label>
                            </div>   
                            <a class="  f-left top30" href="/password/reset" data-toggle="modal" data-target="#forgotPasswordEmail" > @lang('app.forgot_password')</a>
                        </div>

                        
                        {{ Form::close() }}
                    </div>

                    <div class="col-sm-6 col-sm-offset-3 top40" style="background:#f8f8f8">

                      <!-- login sosmed -->
                      <div class="about-title extrabold uppercase color333 width100">
                        <span class="bgfff wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">Atau login dengan</span>
                    </div>
                    @if(get_option('enable_social_login') == 1)

                    <div class="col-sm-12 row-sm-offset-3 socialButtons">

                        @if(get_option('enable_facebook_login') == 1)
                        <div class="col-xs-6">
                            <a href="{{ route('facebook_redirect') }}" class="btn btn-lg btn-block btn-facebook">
                                <i class="fa fa-facebook visible-xs"></i>
                                <span class="hidden-xs"><i class="fa fa-facebook-square"></i> Facebook</span>
                            </a>
                        </div>
                        @endif

                        @if(get_option('enable_google_login') == 1)
                        <div class="col-xs-6">
                            <a href="{{ route('google_redirect') }}" class="btn btn-lg btn-block btn-google" style="color:red">
                                <i class="fa fa-google-plus visible-xs"></i>
                                <span class="hidden-xs"><i class="fa fa-google-plus-square" ></i> Google+</span>
                            </a>
                        </div>
                        @endif

                    </div>
                    @endif
                </div> </div>
                <!-- sosmed login -->
            </div>
        </div>
    </div>
</div>                   
<br><br>
 

<div class="modal fade" id="forgotPasswordEmail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>'send_reset_link']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('app.enter_email_to_reset_password')</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="email" class="control-label">@lang('app.email'):</label>
                    <input type="text" class="form-control" id="email" name="email">
                    <div id="email_info"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                <button type="submit" class="btn btn-primary" id="send_reset_link">@lang('app.send_reset_link')</button>
            </div>
        </form>

    </div>
</div>
</div>
<div class="clearfix"></div>
</div>
</section>
@endsection

@section('page-js')
<script src="assets/contact/jqBootstrapValidation.js"></script> 
<script>
var options = {closeButton : true};
@if(session('error'))
toastr.error('{{ session('error') }}', 'Error!', options)
@endif
</script>
@endsection
